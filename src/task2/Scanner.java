package task2;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class Scanner extends AbstractJob implements Supplier<String> {

    /* значение для иммитации затрат по времени на сканирование */
    private static final int TIME_IN_MILLISECONDES = 1500;
    private String inputData;

    public Scanner(String inputData) {
        this.inputData = inputData;
    }

    @Override
    public String get() {
        try {
            TimeUnit.MILLISECONDS.sleep(TIME_IN_MILLISECONDES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        StringBuilder lines = new StringBuilder();
        for (int j = 0; j < inputData.length() + 4; j++) {
            lines.append(".");
        }
        String line = lines.toString();

        StringBuilder sb = new StringBuilder();

        sb.append(line + "\n");
        sb.append(". ");
        sb.append(inputData);
        sb.append(" .\n");
        sb.append(line + "\n");
        return sb.toString();
    }

    @Override
    public String getJobType() {
        return "<< Сканирование >>";
    }

    @Override
    public boolean isNeedResource() {
        return false;
    }

}
