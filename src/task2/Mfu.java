package task2;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class Mfu {

    /* значение мощности МФУ одновременно выполнять задания */
    private static final int DEFAULT_CONCURRENT_JOB_NUMBER = 2;
    /* значение ресурса картриджа */
    private static final int DEFAULT_CARTRIDGE_RESOURCE_IN_BYTES = 200;

    private static final String TURN_ON_TEMPLATE = "Включение принтера (мощность = %d, ресурс картриджа = %d)...\n";
    private ExecutorService executor;
    private int counter;
    private int printedBytes = 0;
    private int scannedBytes = 0;
    private int cartridgeResource;
    private final Object lock = new Object();

    public Mfu() {
        System.out.println(String.format(
                TURN_ON_TEMPLATE, DEFAULT_CONCURRENT_JOB_NUMBER, DEFAULT_CARTRIDGE_RESOURCE_IN_BYTES)
        );
        executor = Executors.newFixedThreadPool(DEFAULT_CONCURRENT_JOB_NUMBER);
        this.cartridgeResource = DEFAULT_CARTRIDGE_RESOURCE_IN_BYTES;
    }

    public Mfu(int concurrentJobNumber, int cartridgeResource) {
        if (concurrentJobNumber <= 0 || cartridgeResource <= 0) {

            System.out.println(String.format(
                    TURN_ON_TEMPLATE, DEFAULT_CONCURRENT_JOB_NUMBER, DEFAULT_CARTRIDGE_RESOURCE_IN_BYTES)
            );
            executor = Executors.newFixedThreadPool(DEFAULT_CONCURRENT_JOB_NUMBER);
            this.cartridgeResource = DEFAULT_CARTRIDGE_RESOURCE_IN_BYTES;

        } else {

            System.out.println(String.format(
                    TURN_ON_TEMPLATE, concurrentJobNumber, cartridgeResource)
            );
            executor = Executors.newFixedThreadPool(concurrentJobNumber);
            this.cartridgeResource = cartridgeResource;

        }
    }

    public void runTask(Supplier<String> supplier) {
        final AbstractJob job;

        if (supplier instanceof AbstractJob) {
            job = (AbstractJob) supplier;
            job.setJobNumber(++counter);
        } else {
            throw new IllegalArgumentException("Не поддерживаемая функция МФУ.");
        }

        /* отправляем задачу в пул */
        CompletableFuture.supplyAsync(supplier, executor)
                .thenAccept(s -> this.resultInfo(s, job));
    }

    public void turnOff() {
        final String template = "Статистика: (заданий = %d; отсканировано = %d байтов; напечатано = %d байтов; остаток ресурса = %d)";
        executor.shutdown();
        while (!executor.isTerminated()) {
            // nothing
        }
        System.out.println("\nЗавершение работы...");
        System.out.println(String.format(template, counter, scannedBytes, printedBytes, cartridgeResource));
        System.out.println("Выключен");
    }

    private void resultInfo(String result, AbstractJob job) {
        String outputResult = result;

        synchronized (lock) {
            System.out.println("job #" + job.getJobNumber() + " " + job.getJobType());

            if (job.isNeedResource()) {

                int dataLength = outputResult.getBytes().length;

                if (this.cartridgeResource == 0) {

                    System.out.println(" >> картридж пуст << \n");
                    return;

                } else if ((this.cartridgeResource - dataLength) < 0) {

                    int delta = Math.abs(this.cartridgeResource - dataLength);
                    byte[] fragment = new byte[dataLength - delta];
                    System.arraycopy(outputResult.getBytes(), 0, fragment, 0, fragment.length);
                    outputResult = new String(fragment);
                    this.cartridgeResource = 0;
                    printedBytes += outputResult.getBytes().length;
                    outputResult += ("\n~~~ ! картридж пуст ! ~~~\n");

                } else {
                    printedBytes += outputResult.getBytes().length;
                    this.cartridgeResource -= outputResult.getBytes().length;
                }

            } else {
                scannedBytes += outputResult.getBytes().length;
            }

            System.out.println(outputResult);
        }

    }

}
