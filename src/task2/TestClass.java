package task2;

public class TestClass {

    public static void main(String[] args) {

        /* Внимание! В подсчеты байтов выходных данных входит также симолы "." */

        int maxThreads = 3;
        int cartridgeResource = 110;

        Mfu mfu = new Mfu(maxThreads, cartridgeResource);

        mfu.runTask(new Scanner("scan-data"));
        mfu.runTask(new Printer("text to print"));
        mfu.runTask(new Scanner("картинка для сканирования"));
        mfu.runTask(new Printer("123 text to print"));
        mfu.runTask(new Printer("something to print"));
        mfu.runTask(new Scanner("empty page"));
        mfu.runTask(new Printer("something to print more"));

        mfu.turnOff();
    }
}
