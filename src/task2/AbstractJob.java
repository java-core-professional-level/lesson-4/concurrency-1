package task2;

public abstract class AbstractJob {

    private int jobNumber = 0;

    public int getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(int jobNumber) {
        this.jobNumber = jobNumber;
    }

    public abstract String getJobType();

    public abstract boolean isNeedResource();

}
