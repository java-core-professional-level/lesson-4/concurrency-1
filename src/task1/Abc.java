package task1;

public class Abc {

    private static final int NUMBER = 5;
    private final Object lock = new Object();
    private volatile char nextLetter = 'A';

    public void printA() {
        synchronized (lock) {
            for (int j = 0; j < NUMBER; j++) {
                while ('A' != nextLetter) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.print(nextLetter);
                nextLetter = 'B';
                lock.notifyAll();
            }
        }
    }

    public void printB() {
        synchronized (lock) {
            for (int j = 0; j < NUMBER; j++) {
                while ('B' != nextLetter) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.print(nextLetter);
                nextLetter = 'C';
                lock.notifyAll();
            }
        }
    }

    public void printC() {
        synchronized (lock) {
            for (int j = 0; j < NUMBER; j++) {
                while ('C' != nextLetter) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.print(nextLetter);
                nextLetter = 'A';
                lock.notifyAll();
            }
        }
    }


}
