package task1;

public class TestClass {

    public static void main(String[] args) {

        Abc abc = new Abc();
        Thread tA = new Thread(() -> {
           abc.printA();
        });

        Thread tB = new Thread(() -> {
            abc.printB();
        });

        Thread tC = new Thread(() -> {
            abc.printC();
        });

        tA.start();
        tB.start();
        tC.start();

    }

}
